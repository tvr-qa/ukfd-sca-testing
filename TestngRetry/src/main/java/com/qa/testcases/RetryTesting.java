package com.qa.testcases;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.qa.ExtentReportsListeners.ExtentReportListener;
import com.qa.analyzer.ListenerAdapter;
import com.qa.analyzer.RetryAnalyzer;

import junit.framework.Assert;
@Listeners(value = ListenerAdapter.class)

public class RetryTesting extends ExtentReportListener {
	

	@Test(retryAnalyzer = RetryAnalyzer.class)
	public void test1()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium_chrome\\chromedriver_win32 (3)\\Chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		test=extent.createTest("First test");
		driver.get("https://www.netsuite.com/portal/home.shtml");
		driver.findElement(By.xpath("//a[@id='loginBtn']")).click();
		WebElement title=driver.findElement(By.xpath("//title"));
		String title1=title.getText();
		System.out.println(title1);
		if(!driver.findElement(By.xpath("//title")).getText().trim().equals(""))
		{
			
			test.pass("Title is correct");
		}
		else
		{
			test.fail("Title is incorrect");
		}
		
	}
	@AfterMethod
	public void after_Method(ITestResult result)
	{
		if(result.getStatus()==ITestResult.SKIP)
		{
			extent.removeTest(test);
		}
		
	}


}
